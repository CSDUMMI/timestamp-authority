"""
Time Stamp Authority
Copyright (C) 2021 CSDUMMI <csdummi.misquality@simplelogin.co>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
from flask import Flask, request, jsonify
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA3_256
from Crypto.Signature import pkcs1_15

app = Flask(__name__)
privkey = RSA.import_key(open("privkey.pem").read())
privkey_expires = int(open("privkey-expires").read())

@app.route("/timestamp", methods = ["POST"])
def timestamp():
    data = request.form["data"].encode("utf-8")

    time = time.time()

    message = json.dumps({
        "time": time,
        "data": data
    }).encode("utf-8")

    hash = SHA3_256.new(message)
    signature = pkcs1_15.new(privkey).sign(hash)

    return jsonify({
        "time": time,
        "data" : data,
    })

@app.route("/pubkey", methods = ["GET"])
def pubkey():
    return jsonify({
        "pubkey" : privkey.publickey().export_key("PEM"),
        "expires" : privkey_expires
    })
