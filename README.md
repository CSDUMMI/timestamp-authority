# Time Stamp Authority
A Server that creates timestamps of any document
it is provided.

It generates backward timestamps.

##### Backward and Forward Timestamps
- Forward Timestamp: A timestamp that can only be created after a certain event.
- Backward Timestamp: A timestamp that can only be created before a certain event.

### API

#### `/timestamp`
###### Arguments:
- `data`: Data to backward timestamp.

###### Response
```json
{
    "time": "BACKWARD TIMESTAMP",
    "sig": "SIGNATURE OF TIMESTAMP AND DATA"
}
```

#### `/pubkey`
Returns the public key of the TSA.
###### Arguments
###### Response
```json
{
    "publickeys": [
        {
            "pubkey": "PEM of PUBLICKEY",
            "expires": "EXPIRATION TIMESTAMP"
        }
    ]
}
```


### Timestamp Formats
Timestamps are denoted in UNIX Epoch.
